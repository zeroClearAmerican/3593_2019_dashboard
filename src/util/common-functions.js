export function capitalizeFirstLetter(string) {
  return string.replace(/^\w/, (c) => c.toUpperCase())
}
