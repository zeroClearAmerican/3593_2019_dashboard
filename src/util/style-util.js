import * as func from './common-functions'

export function getAllEngines(method, string) {
  return {
    [`Webkit${func.capitalizeFirstLetter(method)}`]: string,
    [`Moz${func.capitalizeFirstLetter(method)}`]: string,
    [`ms${func.capitalizeFirstLetter(method)}`]: string,
    [`O${func.capitalizeFirstLetter(method)}`]: string,
    [method.toLowerCase()]: string
  }
}

export const unselectable = {
  userSelect: 'none',
  WebkitUserSelect: 'none',
  MozUserSelect: 'none',
  msUserSelect: 'none'
}

// Transition
export function transition(timing = 0.3) {
  return `
      -webkit-transition: all ${timing}s cubic-bezier(0.215, 0.61, 0.355, 1);
      -moz-transition: all ${timing}s cubic-bezier(0.215, 0.61, 0.355, 1);
      -ms-transition: all ${timing}s cubic-bezier(0.215, 0.61, 0.355, 1);
      -o-transition: all ${timing}s cubic-bezier(0.215, 0.61, 0.355, 1);
      transition: all ${timing}s cubic-bezier(0.215, 0.61, 0.355, 1);
  `
}

// Transition JSX
export function transitionJsx(timing = 0.3) {
  return getAllEngines(
    'transition',
    `all ${timing}s cubic-bezier(0.215, 0.61, 0.355, 1)`
  )
}
