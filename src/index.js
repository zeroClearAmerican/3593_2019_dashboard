import React from 'react'
import ReactDOM from 'react-dom'
import { ThemeProvider, createTheme, Arwes } from 'arwes'
import Dashboard from './Dashboard'
import './index.css'
import 'semantic-ui-css/semantic.min.css'
import GridImage from './img/glow.png'

const ui = (
  <ThemeProvider theme={{ ...createTheme(), fontFamily: 'Segoe UI' }}>
    <Arwes style={{ fontFamily: 'Segoe UI' }} pattern={GridImage}>
      <Dashboard />
    </Arwes>
  </ThemeProvider>
)

ReactDOM.render(ui, document.getElementById('root'))
