import React, { Component } from 'react'
import { Row, Col } from 'react-grid-system'
import { Icon, InputNumber, Switch } from 'antd'
import { Frame, Words } from 'arwes'
import VerticalGauge from '../components/VerticalGauge'
import ElevatorLevelDiagram from '../components/ElevatorLevelDiagram'
import { transitionJsx } from '../util/style-util'
import RobotGyroDial from '../components/RobotGyroDial'
import StatusBox from '../components/StatusBox'
import SolenoidLED from '../components/SolenoidLED'
import VisionYawCompass from '../components/VisionYawCompass'
import PickupSideView from '../components/PickupSideView'

// const leftPowerGaugeProps = {
//   min: 0,
//   max: 30,
//   width: 166,
//   valueUnits: 'W',
//   colors: [
//     { color: '#C13838', percent: '0%' },
//     { color: '#628FED', percent: '30%' },
//     { color: '#3CF27A', percent: '80%' }
//   ]
// }

export default class Main extends Component {
  GetDemoState = () => {
    return {
      allianceColor: Math.random() > 0.5 ? 'Blue' : 'Red',
      matchTime: Math.random() * 135,
      robotMode: 'Teleop',
      elevator: {
        elevatorPosition: Math.random() * 29800,
        wristAngle: Math.random() * 200,
        wristLowLimit: Math.random() > 0.8,
        wristHighLimit: Math.random() < 0.2
      },
      pid: {
        drive_low_F: 0.01
      },
      drive: {
        leftMag: Math.random(),
        rightMag: Math.random(),
        chassisYaw: Math.random() * 360,
        alignmentEngaged: Math.random() > 0.5,
        transmission: Math.random() > 0.5 ? 'high' : 'low'
      },
      vision: {
        tapePresent: Math.random() > 0.2,
        tapeYaw:
          Math.random() < 0.5 ? -(Math.random() * 50) : Math.random() * 50
      },
      power: {
        batteryVoltage: Math.random() * 2 + 11,
        brownOut: Math.random() > 0.5
      },
      solenoids: [
        Math.random() > 0.5,
        Math.random() > 0.5,
        Math.random() > 0.5,
        Math.random() > 0.5,
        Math.random() > 0.5,
        Math.random() > 0.5,
        Math.random() > 0.5,
        Math.random() > 0.5
      ]
    }
  }

  NumberInputDefaultProps = (robotState, fpid, stateName) => {
    let value = 0
    let disabled = true
    if (robotState.pid && robotState.pid[stateName]) {
      // if actual robot data has come in,
      // check to see if we've set a new value for this stateName
      // if so, display the newest value
      // if not, display the robot's default value
      value = this.props.pidNewValues[stateName] || robotState.pid[stateName]
      disabled = false
    }

    disabled = this.props.useDefaultPid === true ? true : disabled

    return {
      formatter: val => `${fpid}: ${val}`,
      value,
      disabled,
      step: 0.001,
      size: 'large',
      placeHolder: 'I',
      style: {
        color: disabled ? '#666' : 'unset',
        backgroundColor: 'rgba(255, 255, 255, 0.1)',
        marginRight: '10px'
      }
    }
  }

  render() {
    let {
      demoMode,
      robotState,
      useDefaultPid,
      SwitchDefaultPid,
      UpdatePidTestingValue
    } = this.props

    if (demoMode) {
      robotState = this.GetDemoState()
    }

    let modeColor = ''
    let modeText = 'Not Connected'
    switch (robotState.robotMode || 'disabled') {
      default:
      case 'disabled': {
        modeColor = 'red'
        modeText = 'Disabled'
        break
      }
      case 'auto': {
        modeColor = '#E2EC08'
        modeText = 'Auto'
        break
      }
      case 'teleop': {
        modeColor = '#3FEC08'
        modeText = 'Teleop'
        break
      }
    }

    let matchTimeString = '00:00'
    if (robotState.matchTime) {
      const minutesRemaining = (
        '0' + Math.floor(robotState.matchTime / 60)
      ).slice(-2)
      const secondsRemaining = (
        '0' + Math.round(robotState.matchTime % 60)
      ).slice(-2)
      matchTimeString = `${minutesRemaining}:${secondsRemaining}`
    }

    return (
      <Row>
        <Col md={8} id="left_column">
          <Frame
            show
            animate
            level={2}
            corners={4}
            layer={robotState.allianceColor === 'Blue' ? 'primary' : 'alert'}
          >
            <div
              style={{
                width: '100%',
                height: '400px',
                padding: '0px 5px'
              }}
            >
              <div>
                <Words style={{ fontSize: '1.5em' }} animate>
                  Drive
                </Words>
              </div>
              <VerticalGauge
                min={0}
                max={1}
                height={330}
                width="80px"
                colors={[
                  { color: '#3CF27A', percent: '0%' },
                  { color: '#628FED', percent: '60%' },
                  { color: '#8D6ABF', percent: '100%' }
                ]}
                value={robotState.drive ? robotState.drive.leftMag : 0}
                style={{ display: 'inline-block', marginRight: '5px' }}
              />
              <div
                style={{
                  position: 'absolute',
                  width: '310px',
                  height: '350px',
                  display: 'inline-block',
                  left: 'calc(50% - 155px)'
                }}
              >
                <RobotGyroDial
                  angle={robotState.drive ? robotState.drive.chassisYaw : 0}
                  width="300px"
                />
              </div>
              <VerticalGauge
                min={0}
                max={1}
                height={330}
                width="80px"
                colors={[
                  { color: '#3CF27A', percent: '0%' },
                  { color: '#628FED', percent: '60%' },
                  { color: '#8D6ABF', percent: '100%' }
                ]}
                value={robotState.drive ? robotState.drive.rightMag : 0}
                style={{
                  display: 'inline-block',
                  marginRight: '5px',
                  float: 'right'
                }}
              />
            </div>
            <div
              style={{ padding: '0px 10px 10px 10px', position: 'relative' }}
            >
              <StatusBox
                state={
                  robotState.drive
                    ? robotState.drive.alignmentEngaged
                      ? 'locked'
                      : 'unlocked'
                    : 'unlocked'
                }
                states={{
                  locked: {
                    backColor: '#F41',
                    content: (
                      <div style={{ fontSize: '2em' }}>
                        <Icon
                          type="lock"
                          style={{ color: 'black', marginRight: '5px' }}
                        />
                        Locked On
                      </div>
                    )
                  },
                  unlocked: {
                    backColor: '#628FED',
                    content: (
                      <div style={{ fontSize: '2em' }}>
                        <Icon
                          type="unlock"
                          style={{
                            color: 'black',
                            marginRight: '5px'
                          }}
                        />
                        Unlocked
                      </div>
                    )
                  }
                }}
                width="49%"
                style={{ display: 'inline-block' }}
              />
              <StatusBox
                state={
                  robotState.drive
                    ? robotState.drive.transmission
                      ? 'high'
                      : 'low'
                    : 'low'
                }
                states={{
                  high: {
                    backColor: '#0F0',
                    content: (
                      <div style={{ fontSize: '2em' }}>
                        <Icon
                          type="double-right"
                          style={{
                            color: 'black',
                            marginRight: '5px',
                            transform: 'rotate(-90deg)'
                          }}
                        />
                        High Gear
                      </div>
                    )
                  },
                  low: {
                    backColor: '#EB0',
                    content: (
                      <div style={{ fontSize: '2em' }}>
                        <Icon
                          type="right"
                          style={{
                            color: 'black',
                            marginRight: '5px',
                            transform: 'rotate(-90deg)'
                          }}
                        />
                        Low Gear
                      </div>
                    )
                  }
                }}
                width="49%"
                style={{ display: 'inline-block', marginLeft: '10px' }}
              />
            </div>
          </Frame>
          <Frame
            show
            animate
            level={2}
            corners={4}
            layer={robotState.allianceColor === 'Blue' ? 'primary' : 'alert'}
            style={{ marginTop: '10px' }}
          >
            <Row>
              <Col md={8} style={{ padding: '5px', marginRight: '0px' }}>
                <div
                  style={{
                    fontSize: '2.5em',
                    textAlign: 'center',
                    marginBottom: '10px'
                  }}
                >
                  <div>Solenoids</div>
                  <div>
                    <span>1</span>
                    <SolenoidLED
                      on={
                        robotState.solenoids ? robotState.solenoids[0] : false
                      }
                      reverseRotate
                    />
                    <SolenoidLED
                      on={
                        robotState.solenoids ? robotState.solenoids[4] : false
                      }
                    />
                    <span>5</span>
                  </div>
                  <div>
                    <span>2</span>
                    <SolenoidLED
                      on={
                        robotState.solenoids ? robotState.solenoids[1] : false
                      }
                      reverseRotate
                    />
                    <SolenoidLED
                      on={
                        robotState.solenoids ? robotState.solenoids[5] : false
                      }
                    />
                    <span>6</span>
                  </div>
                  <div>
                    <span>3</span>
                    <SolenoidLED
                      on={
                        robotState.solenoids ? robotState.solenoids[2] : false
                      }
                      reverseRotate
                    />
                    <SolenoidLED
                      on={
                        robotState.solenoids ? robotState.solenoids[6] : false
                      }
                    />
                    <span>7</span>
                  </div>
                  <div>
                    <span>4</span>
                    <SolenoidLED
                      on={
                        robotState.solenoids ? robotState.solenoids[3] : false
                      }
                      reverseRotate
                    />
                    <SolenoidLED
                      on={
                        robotState.solenoids ? robotState.solenoids[7] : false
                      }
                    />
                    <span>8</span>
                  </div>
                </div>
              </Col>
              <Col
                md={16}
                style={{
                  textAlign: 'left',
                  paddingTop: '10px',
                  paddingLeft: '5px',
                  fontSize: '18px'
                }}
              >
                <div
                  style={{
                    fontWeight: '500',
                    borderBottom: '1px solid #42B9C2',
                    marginBottom: '5px',
                    width: '97%'
                  }}
                >
                  Drive low-gear PID
                  <Switch
                    disabled={robotState.pid === undefined}
                    checked={useDefaultPid}
                    onChange={val => SwitchDefaultPid(val)}
                    style={{ float: 'right' }}
                  />
                </div>
                <div style={{ marginBottom: '15px' }}>
                  <InputNumber
                    {...this.NumberInputDefaultProps(
                      robotState,
                      'F',
                      'drive_low_F'
                    )}
                    onChange={val => UpdatePidTestingValue('drive_low_F', val)}
                  />
                  <InputNumber
                    {...this.NumberInputDefaultProps(
                      robotState,
                      'P',
                      'drive_low_P'
                    )}
                    onChange={val => UpdatePidTestingValue('drive_low_P', val)}
                  />
                  <InputNumber
                    {...this.NumberInputDefaultProps(
                      robotState,
                      'I',
                      'drive_low_I'
                    )}
                    onChange={val => UpdatePidTestingValue('drive_low_I', val)}
                  />
                  <InputNumber
                    {...this.NumberInputDefaultProps(
                      robotState,
                      'D',
                      'drive_low_D'
                    )}
                    onChange={val => UpdatePidTestingValue('drive_low_D', val)}
                  />
                </div>
                <div
                  style={{
                    fontWeight: '500',
                    borderBottom: '1px solid #42B9C2',
                    marginBottom: '5px',
                    width: '97%'
                  }}
                >
                  Drive high-gear PID
                </div>
                <div style={{ marginBottom: '15px' }}>
                  <InputNumber
                    {...this.NumberInputDefaultProps(
                      robotState,
                      'F',
                      'drive_high_F'
                    )}
                    onChange={val => UpdatePidTestingValue('drive_high_F', val)}
                  />
                  <InputNumber
                    {...this.NumberInputDefaultProps(
                      robotState,
                      'P',
                      'drive_high_P'
                    )}
                    onChange={val => UpdatePidTestingValue('drive_high_P', val)}
                  />
                  <InputNumber
                    {...this.NumberInputDefaultProps(
                      robotState,
                      'I',
                      'drive_high_I'
                    )}
                    onChange={val => UpdatePidTestingValue('drive_high_I', val)}
                  />
                  <InputNumber
                    {...this.NumberInputDefaultProps(
                      robotState,
                      'D',
                      'drive_high_D'
                    )}
                    onChange={val => UpdatePidTestingValue('drive_high_D', val)}
                  />
                </div>
                <div
                  style={{
                    fontWeight: '500',
                    borderBottom: '1px solid #42B9C2',
                    marginBottom: '5px',
                    width: '97%'
                  }}
                >
                  Elevator PID
                </div>
                <div
                  style={{
                    marginBottom: '15px'
                  }}
                >
                  <InputNumber
                    {...this.NumberInputDefaultProps(
                      robotState,
                      'F',
                      'elevator_F'
                    )}
                    onChange={val => UpdatePidTestingValue('elevator_F', val)}
                  />
                  <InputNumber
                    {...this.NumberInputDefaultProps(
                      robotState,
                      'P',
                      'elevator_P'
                    )}
                    onChange={val => UpdatePidTestingValue('elevator_P', val)}
                  />
                  <InputNumber
                    {...this.NumberInputDefaultProps(
                      robotState,
                      'I',
                      'elevator_I'
                    )}
                    onChange={val => UpdatePidTestingValue('elevator_I', val)}
                  />
                  <InputNumber
                    {...this.NumberInputDefaultProps(
                      robotState,
                      'D',
                      'elevator_D'
                    )}
                    onChange={val => UpdatePidTestingValue('elevator_D', val)}
                  />
                </div>
              </Col>
            </Row>
          </Frame>
        </Col>
        <Col md={8} id="middle_column">
          <Row style={{ marginBottom: '10px' }}>
            <Frame
              show
              animate
              level={2}
              corners={4}
              layer={robotState.allianceColor === 'Blue' ? 'primary' : 'alert'}
              style={{
                width: '100%'
              }}
            >
              <div style={{ width: '100%', paddingBottom: '15px' }}>
                <div
                  style={{
                    textAlign: 'center',
                    width: '100%',
                    marginBottom: '15px'
                  }}
                >
                  <div
                    style={{
                      fontFamily: 'Consolas',
                      fontSize: '3em',
                      color: '#3CE4F2',
                      fontWeight: 400
                    }}
                  >
                    Match Time: {matchTimeString}
                  </div>
                </div>
                <Row>
                  <Col
                    md={12}
                    style={{
                      textAlign: 'right',
                      borderRight: '1px solid #3CE4F2'
                    }}
                  >
                    <h2
                      style={{
                        fontFamily: 'Segoe UI',
                        color: '#3CE4F2',
                        fontWeight: 400
                      }}
                    >
                      <span
                        style={{
                          color:
                            robotState.allianceColor === 'Blue'
                              ? '#009CFF'
                              : 'red',
                          padding: '0px 5px',
                          borderRadius: '3px',
                          ...transitionJsx()
                        }}
                      >
                        {robotState.allianceColor === 'Blue'
                          ? 'Blue Alliance'
                          : 'Red Alliance'}
                      </span>
                    </h2>
                  </Col>
                  <Col md={12} style={{ textAlign: 'left' }}>
                    <h2
                      style={{
                        fontFamily: 'Segoe UI',
                        color: '#3CE4F2',
                        fontWeight: 400
                      }}
                    >
                      <span style={{ color: modeColor }}>{modeText}</span>
                    </h2>
                  </Col>
                </Row>
              </div>
            </Frame>
          </Row>
          <Row>
            <Frame
              show
              animate
              level={2}
              corners={4}
              layer={robotState.allianceColor === 'Blue' ? 'primary' : 'alert'}
              style={{
                width: '100%',
                padding: '10px'
              }}
            >
              <StatusBox
                state={
                  robotState.vision
                    ? robotState.vision.tapePresent
                      ? 'present'
                      : 'absent'
                    : 'absent'
                }
                states={{
                  present: {
                    backColor: '#97FF12',
                    content: (
                      <div style={{ fontSize: '2em' }}>
                        <Icon
                          type="scan"
                          style={{ color: 'black', marginRight: '5px' }}
                        />
                        Target Visible
                      </div>
                    )
                  },
                  absent: {
                    backColor: '#FF5733',
                    content: (
                      <div style={{ fontSize: '2em' }}>
                        <Icon
                          type="eye-invisible"
                          style={{
                            color: 'black',
                            marginRight: '5px'
                          }}
                        />
                        Target Missing
                      </div>
                    )
                  }
                }}
                width="80%"
                height="60px"
                style={{ marginLeft: 'auto', marginRight: 'auto' }}
              />
              <VisionYawCompass
                targetSeen={
                  robotState.vision ? robotState.vision.tapePresent : false
                }
                targetYaw={robotState.vision ? robotState.vision.tapeYaw : 0}
              />
              {robotState.vision ? (
                <div
                  style={{
                    marginTop: '10px',
                    fontSize: '26px',
                    fontWeight: '500',
                    textAlign: 'center'
                  }}
                >
                  {robotState.vision.tapePresent ? (
                    <div>
                      Angle to target:
                      <span
                        style={{
                          marginLeft: '5px',
                          color:
                            Math.abs(robotState.vision.tapeYaw) <= 3
                              ? '#0F0'
                              : Math.abs(robotState.vision.tapeYaw) <= 10
                              ? '#FF0'
                              : '#F00'
                        }}
                      >
                        {robotState.vision.tapeYaw}
                      </span>
                    </div>
                  ) : (
                    <span>Target out of range</span>
                  )}
                </div>
              ) : (
                <div
                  style={{
                    marginTop: '10px',
                    fontSize: '20px',
                    fontWeight: '500',
                    textAlign: 'center'
                  }}
                >
                  No vision data
                </div>
              )}
            </Frame>
          </Row>
          <Row>
            <Frame
              show
              animate
              level={2}
              corners={4}
              layer={robotState.allianceColor === 'Blue' ? 'primary' : 'alert'}
              style={{
                width: '100%',
                padding: '10px',
                marginTop: '10px'
              }}
            >
              <Row style={{ height: '251px', padding: '5px' }}>
                <Col md={8} style={{ textAlign: 'center' }}>
                  <div
                    style={{
                      fontSize: '20px',
                      fontWeight: '500',
                      marginBottom: '10px'
                    }}
                  >
                    Battery Volts:
                  </div>
                  <VerticalGauge
                    min={0}
                    max={14}
                    height={190}
                    width="210px"
                    valueUnits=" V"
                    value={
                      robotState.power ? robotState.power.batteryVoltage : 0
                    }
                    colors={[
                      { color: '#C13838', percent: '0%' },
                      { color: '#E4E444', percent: '80%' },
                      { color: '#3CF27A', percent: '90%' }
                    ]}
                  />
                </Col>
                <Col md={8} style={{ textAlign: 'center' }}>
                  <img
                    src={require('../img/pdp_blue.png')}
                    alt="pdp_blue"
                    style={{
                      display: 'block',
                      width: '160px',
                      marginLeft: 'auto',
                      marginRight: 'auto'
                    }}
                  />
                </Col>
                <Col md={8} style={{ textAlign: 'center' }}>
                  <StatusBox
                    state={
                      robotState.power
                        ? robotState.power.brownOut
                          ? 'yes'
                          : 'no'
                        : 'no'
                    }
                    states={{
                      no: {
                        backColor: '#82BD71',
                        content: (
                          <div style={{ fontSize: '2em' }}>
                            <Icon
                              type="thunderbolt"
                              style={{ color: 'black', marginRight: '5px' }}
                            />
                            Nominal
                          </div>
                        )
                      },
                      yes: {
                        backColor: '#DA2727',
                        content: (
                          <div style={{ fontSize: '2em' }}>
                            <Icon
                              type="fire"
                              style={{
                                color: 'black',
                                marginRight: '5px'
                              }}
                            />
                            BROWNOUT
                          </div>
                        )
                      }
                    }}
                    width="100%"
                    height="230px"
                    style={{
                      display: 'inline-block',
                      marginLeft: '-30px',
                      fontSize: '0.9em'
                    }}
                  />
                </Col>
              </Row>
            </Frame>
          </Row>
        </Col>
        <Col md={8} id="right_column">
          <Frame
            show
            animate
            level={2}
            corners={4}
            layer={robotState.allianceColor === 'Blue' ? 'primary' : 'alert'}
            style={{ marginBottom: '10px' }}
          >
            <div style={{ width: '100%', height: '100%', padding: '5px' }}>
              <div>
                <Words style={{ fontSize: '1.5em' }} animate>
                  Elevator
                </Words>
              </div>
              <Row>
                <Col md={10}>
                  <div
                    style={{
                      position: 'relative',
                      width: '100%',
                      height: '800px'
                    }}
                  >
                    <PickupSideView
                      width="250px"
                      angle={
                        robotState.elevator ? robotState.elevator.wristAngle : 0
                      }
                      style={{
                        position: 'absolute',
                        marginLeft: '300px',
                        marginTop: '150px',
                        transform: 'scaleX(-1)'
                      }}
                    />
                    <ElevatorLevelDiagram
                      level={
                        robotState.elevator
                          ? robotState.elevator.elevatorPosition
                          : 0
                      }
                      width={400}
                      style={{ position: 'absolute', marginLeft: '20%' }}
                    />
                  </div>
                </Col>
              </Row>
            </div>
          </Frame>
        </Col>
      </Row>
    )
  }
}
