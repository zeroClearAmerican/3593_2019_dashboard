import styled from 'styled-components'
import * as style from '../util/style-util'

const CustomButton = styled.div`
  ${style.transition(0.5)}
  display: inline-block;
  text-align: center;
  cursor: pointer;
  padding: 5px;
  border-radius: 1px;
  background-color: ${(props) => props.background || '#212425'};

  &:hover {
    background-color: ${(props) => props.hoverBackground};
    color: ${(props) => props.hoverForeground};
  }
`

export default CustomButton
