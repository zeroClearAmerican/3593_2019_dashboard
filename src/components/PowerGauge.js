import React, { Component } from 'react'
import HorizontalGauge from './HorizontalGauge'

export default class PowerGauge extends Component {
  render() {
    const { channel } = this.props

    if (this.props.rtl) {
      return (
        <div style={{ lineHeight: '30px', height: '30px', width: '100%' }}>
          <div
            style={{
              display: 'inline-block',
              fontSize: '2em',
              marginRight: '5px',
              float: 'left'
            }}
          >
            {channel}
          </div>
          <HorizontalGauge
            {...this.props}
            style={{
              display: 'inline-block',
              marginRight: 'auto',
              marginLeft: 'auto',
              float: 'right'
            }}
          />
        </div>
      )
    } else {
      return (
        <div style={{ lineHeight: '30px', height: '30px', width: '100%' }}>
          <HorizontalGauge
            {...this.props}
            style={{
              display: 'inline-block',
              marginRight: 'auto',
              marginLeft: 'auto',
              float: 'left'
            }}
          />
          <div
            style={{
              display: 'inline-block',
              fontSize: '2em',
              position: 'absolute',
              marginLeft: '5px',
              float: 'right'
            }}
          >
            {channel}
          </div>
        </div>
      )
    }
  }
}
