import React, { Component } from 'react'
import { transitionJsx } from '../util/style-util'

export default class VisionYawCompass extends Component {
  render() {
    const { targetSeen, targetYaw } = this.props
    const scaledYaw = targetYaw * 3.5

    return (
      <div
        style={{
          position: 'relative',
          width: '100%',
          height: '40px',
          backgroundColor: '#111',
          border: '1px solid #00DFFF'
        }}
      >
        <div
          id="center_horizontalLine"
          style={{
            position: 'absolute',
            zIndex: '200',
            marginLeft: '2%',
            marginTop: '19px',
            width: '96%',
            height: '1px',
            borderBottom: '1px dashed #1DB21D'
          }}
        />
        <div
          id="center_reticle"
          style={{
            position: 'absolute',
            zIndex: '300',
            marginLeft: 'calc(50% - 2px)',
            marginTop: '2px',
            width: '4px',
            height: '32px',
            backgroundColor: '#0F0',
            borderRadius: '2px'
          }}
        />
        {targetSeen ? (
          <div
            id="target"
            style={{
              position: 'absolute',
              zIndex: '100',
              marginLeft: `calc(50% + ${scaledYaw - 33}px)`,
              height: '40px',
              ...transitionJsx(1.0)
            }}
          >
            <div
              id="left_tape"
              style={{
                position: 'absolute',
                marginTop: '4px',
                width: '12px',
                height: '28px',
                backgroundColor: '#FFF',
                transform: 'skewX(-14.5deg)'
              }}
            />
            <div
              id="right_tape"
              style={{
                position: 'absolute',
                marginLeft: '55px',
                marginTop: '4px',
                width: '12px',
                height: '28px',
                backgroundColor: '#FFF',
                transform: 'skewX(14.5deg)'
              }}
            />
          </div>
        ) : null}
      </div>
    )
  }
}
