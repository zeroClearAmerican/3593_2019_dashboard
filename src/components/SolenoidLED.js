import React, { Component } from 'react'
import { Icon } from 'antd'
import { transitionJsx } from '../util/style-util'

export default class SolenoidLED extends Component {
  render() {
    const {
      on = true,
      onColor = '#48E444',
      offColor = '#E44444',
      style,
      reverseRotate
    } = this.props
    const degRotation = reverseRotate ? '-45' : '45'

    return (
      <Icon
        type="plus-circle"
        style={{
          color: on ? onColor : offColor,
          margin: '5px',
          ...style,
          ...transitionJsx(),
          transform: on ? 'rotate(0deg)' : `rotate(${degRotation}deg)`
        }}
      />
    )
  }
}
