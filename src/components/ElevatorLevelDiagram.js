import React, { Component } from 'react'
import ElevatorBase from '../img/themed_robot/elevator_base_transparent.png'
import ElevatorSecondStage from '../img/themed_robot/elevator_second_stage_transparent.png'
import ElevatorCarriage from '../img/themed_robot/elevator_carriage_transparent.png'
import { transitionJsx } from '../util/style-util'

const ev_bottom_align = {
  mid: 30,
  carriage: 20
}

const ev_left_align = {
  mid: 42,
  carriage: 30
}

const ev_lesserWidthCalcCoeff = 0.4
const ev_heightWidthCalcCoeff = 1.9

export default class ElevatorLevelDiagram extends Component {
  shouldComponentUpdate() {
    // TODO: Modify this to only update every 200ms
    return true
  }

  render() {
    const { level, style, width = 400 } = this.props

    const levelCoeff = level * 0.01308724832214765100671140939598
    const midLevelOffset = levelCoeff / 2 + ev_bottom_align.mid
    const carriageBottomOffset = levelCoeff + ev_bottom_align.carriage

    return (
      <div
        style={{
          height: `${width * ev_heightWidthCalcCoeff}px`,
          width: `${width}px`,
          position: 'relative',
          ...style
        }}
      >
        <img
          alt="ev_base"
          src={ElevatorBase}
          width={`${width}px`}
          style={{
            position: 'absolute',
            zIndex: 20,
            bottom: '0px',
            filter: 'brightness(110%)'
          }}
        />
        <img
          alt="ev_second_stage"
          src={ElevatorSecondStage}
          width={`${width * ev_lesserWidthCalcCoeff}px`}
          style={{
            position: 'absolute',
            zIndex: 30,
            bottom: `${midLevelOffset}px`,
            left: `${ev_left_align.mid}px`,
            filter: 'brightness(110%)',
            ...transitionJsx(1.0)
          }}
        />
        <img
          alt="ev_carriage"
          src={ElevatorCarriage}
          width={`${width * ev_lesserWidthCalcCoeff}px`}
          style={{
            position: 'absolute',
            zIndex: 40,
            bottom: `${carriageBottomOffset}px`,
            left: `${ev_left_align.carriage}px`,
            filter: 'brightness(110%)',
            ...transitionJsx(1.0)
          }}
        />
      </div>
    )
  }
}
