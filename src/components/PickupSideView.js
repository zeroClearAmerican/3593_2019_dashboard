import React, { Component } from 'react'
import { transitionJsx } from '../util/style-util'

export default class PickupSideView extends Component {
  render() {
    const { width, angle, style } = this.props
    const correctedAngle = angle - 60

    return (
      <div style={{ ...style, width }}>
        <img
          src={require('../img/themed_robot/top_bar.png')}
          alt="wrist_side"
          style={{
            position: 'absolute',
            width,
            marginTop: '2px',
            ...transitionJsx(),
            transformOrigin: '8% 45%',
            transform: `rotate(${correctedAngle - 20}deg)`
          }}
        />
        <img
          src={require('../img/themed_robot/vertical_bar.png')}
          alt="wrist_side"
          style={{
            position: 'absolute',
            width: '25px',
            marginLeft: '5px',
            marginTop: '5px',
            ...transitionJsx()
          }}
        />
        <img
          src={require('../img/themed_robot/scoop.png')}
          alt="wrist_side"
          style={{
            position: 'absolute',
            width,
            marginTop: '145px',
            ...transitionJsx(),
            transformOrigin: '6% 77%',
            transform: `rotate(${correctedAngle}deg)`
          }}
        />
      </div>
    )
  }
}
