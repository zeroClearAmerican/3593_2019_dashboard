import React, { Component } from 'react'
import { transitionJsx } from '../util/style-util'

export default class HorizontalGauge extends Component {
  render() {
    const {
      min,
      max,
      colors,
      width = 200,
      height = '30px',
      value,
      valueUnits,
      style,
      rtl
    } = this.props

    let gradient = 'linear-gradient('
    if (rtl) {
      gradient += 'to right,'
      gradient += colors.map((color) => {
        return ` ${color.color} ${color.percent}`
      })
      gradient += ')'
    } else {
      gradient += 'to right,'
      gradient += colors.map((color) => {
        return ` ${color.color} ${color.percent}`
      })
      gradient += ')'
    }

    const shadeWidth = width - Math.abs((value / (max - min)) * width)
    let shadeLeft = 0
    if (rtl) {
      shadeLeft = width - shadeWidth
    }

    return (
      <div
        className="gauge-back"
        style={{
          width: `${width}px`,
          height,
          backgroundImage: gradient,
          borderRadius: '4px',
          position: 'relative',
          ...style
        }}
      >
        <div
          className="gauge-shade"
          ref={(n) => (this.shade = n)}
          style={{
            position: 'absolute',
            top: '0px',
            left: `${shadeLeft}px`,
            zIndex: 10,
            width: `${shadeWidth}px`,
            height,
            backgroundColor: '#222',
            borderRadius: '2px',
            ...transitionJsx()
          }}
        />
        <div
          className="gauge-text"
          style={{
            position: 'absolute',
            width: `${width}px`,
            height,
            top: '0px',
            left: '0px',
            lineHeight: height,
            overflow: 'hidden',
            zIndex: 10,
            color: shadeWidth / width > 0.48 ? 'white' : 'black',
            textAlign: 'center'
          }}
        >
          {`${value}${valueUnits ? valueUnits : ''}`}
        </div>
      </div>
    )
  }
}
