import React, { Component } from 'react'
import RobotAerialView from '../img/themed_robot/robot_aerial_view_transparent.png'
import { transitionJsx, getAllEngines } from '../util/style-util'

export default class RobotGyroDial extends Component {
  render() {
    return (
      <img
        alt="robot_aerial"
        src={RobotAerialView}
        width={this.props.width}
        style={{
          transform: `rotate(${this.props.angle}deg)`,
          ...getAllEngines('filter', 'brightness(110%)'),
          ...transitionJsx(0.2)
        }}
      />
    )
  }
}
