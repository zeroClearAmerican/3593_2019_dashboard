import React, { Component } from 'react'
import { transitionJsx } from '../util/style-util'

export default class VerticalGauge extends Component {
  render() {
    const {
      min,
      max,
      colors,
      width = '40px',
      height = 200,
      value,
      valueUnits,
      style
    } = this.props

    let gradient = 'linear-gradient(to top,'
    gradient += colors.map((color) => {
      return ` ${color.color} ${color.percent}`
    })
    gradient += ')'

    const shadeHeight = height - Math.abs((value / (max - min)) * height)

    return (
      <div
        className="gauge-back"
        style={{
          width,
          height: `${height}px`,
          backgroundImage: gradient,
          borderRadius: '4px',
          position: 'relative',
          ...style
        }}
      >
        <div
          className="gauge-shade"
          ref={(n) => (this.shade = n)}
          style={{
            position: 'absolute',
            top: '0px',
            left: '0px',
            zIndex: 10,
            width,
            height: `${shadeHeight}px`,
            backgroundColor: '#222',
            borderRadius: '2px',
            ...transitionJsx()
          }}
        />
        <div
          className="gauge-text"
          style={{
            position: 'absolute',
            width,
            height: `${height}px`,
            top: '0px',
            left: '0px',
            lineHeight: `${height * 0.8}px`,
            overflow: 'hidden',
            zIndex: 10,
            color: shadeHeight / height > 0.4 ? 'white' : 'black',
            textAlign: 'center'
          }}
        >
          {`${value}${valueUnits ? valueUnits : ''}`}
        </div>
      </div>
    )
  }
}
