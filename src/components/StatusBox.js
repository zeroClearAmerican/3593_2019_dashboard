import React, { Component } from 'react';
import { transitionJsx } from '../util/style-util';

export default class StatusBox extends Component {
  render() {
    const {
      state,
      states,
      height = '70px',
      width = '170px',
      style
    } = this.props;

    return (
      <div
        style={{
          ...transitionJsx(),
          width,
          height,
          lineHeight: height,
          color: 'black',
          textAlign: 'center',
          borderRadius: '3px',
          backgroundColor: states[state].backColor,
          marginBottom: '5px',
          ...style
        }}
      >
        {states[state].content}
      </div>
    );
  }
}
