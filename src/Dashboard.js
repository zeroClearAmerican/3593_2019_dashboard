import React, { Component } from 'react'
import { Icon, Segment, Sidebar } from 'semantic-ui-react'
import { setConfiguration, Container, Row, Col } from 'react-grid-system'
import Main from './Pages/Main'
import { Switch } from 'antd'

setConfiguration({
  defaultScreenClass: 'md',
  gridColumns: 24
})

export default class Dashboard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      demoMode: false,
      robotConnected: false,
      useDefaultPid: true,
      robotState: {},
      pidValues: {}
    }
  }

  componentWillMount() {
    this.refreshSocketConnection()
  }

  componentWillUnmount() {
    if (this.socket) {
      console.log('closing websocket')
      this.socket.onclose = () => {}
      this.socket.close()
    }
  }

  refreshSocketConnection = () => {
    this.socket = new WebSocket('ws://10.35.93.2:5800')
    this.socket.onopen = () => {
      console.log('WS connection started')
      this.setState({ robotConnected: true })
      this.socket.send('ping')

      this.socketTimeout = window.setInterval(() => {
        if (
          this.state.useDefaultPid ||
          Object.keys(this.state.pidValues).length === 0
        ) {
          this.socket.send('ping')
          return
        }
        this.socket.send(JSON.stringify(this.state.pidValues))
      }, 200)
    }

    this.socket.onclose = () => {
      console.log('WS connection failed')
      this.setState({ robotConnected: false })
      window.clearTimeout(this.socketTimeout)
    }

    this.socket.onmessage = this.socketDataReceived
  }

  socketDataReceived = event => {
    try {
      const parsedRobotstate = JSON.parse(event.data)
      // console.log(event);
      this.setState({
        robotState: parsedRobotstate
      })
    } catch (ex) {}
  }

  UpdatePidTestingValue = (valueName, value) => {
    this.setState({
      pidValues: {
        ...this.state.pidValues,
        [valueName]: value
      }
    })
  }

  render() {
    const { demoMode, robotConnected, robotState, useDefaultPid } = this.state
    console.log('PID testing values:', this.state.pidValues)

    return (
      <Sidebar.Pushable
        as={Segment}
        style={{ margin: '0px', background: 'unset' }}
      >
        <Sidebar.Pusher dimmed={false}>
          <Container
            fluid
            className="outer_container"
            style={{
              margin: '0px',
              paddingLeft: '10px',
              paddingRight: '10px',
              height: '100%'
            }}
          >
            <Row className="topRow" style={{ height: '5vh' }}>
              <Col md={4}>
                <div
                  style={{
                    padding: '5px',
                    fontSize: '1em',
                    display: 'inline-block'
                  }}
                >
                  <Icon
                    name="sync alternate"
                    style={{ marginRight: '5px', cursor: 'pointer' }}
                    onClick={this.refreshSocketConnection}
                  />
                  <span
                    style={{
                      color: robotConnected ? '#3BD546' : '#E64942'
                    }}
                  >
                    {robotConnected ? 'Connected' : 'Disconnected'}
                  </span>
                </div>
              </Col>
              <Col md={18} />
              <Col md={2} style={{ padding: '5px', fontSize: '0.9em' }}>
                Demo Mode
                <Switch
                  onChange={v => this.setState({ demoMode: v })}
                  style={{ marginLeft: '5px' }}
                />
              </Col>
            </Row>

            <Main
              robotState={robotState}
              demoMode={demoMode}
              useDefaultPid={useDefaultPid}
              SwitchDefaultPid={val => this.setState({ useDefaultPid: val })}
              pidNewValues={this.state.pidValues}
              UpdatePidTestingValue={this.UpdatePidTestingValue}
            />
          </Container>
        </Sidebar.Pusher>
      </Sidebar.Pushable>
    )
  }
}
