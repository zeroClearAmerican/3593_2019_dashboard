## Overview

This is a websocket-driven Dashboard for 3593's 2019 competition robot that displays robot telemetry and status in a browser window.
The project is written in React and uses the browser's Websocket class to open a connection to the robot at any update rate needed. 

Please excuse any poorly written code or commits, as much of the most recent work on this project was done at or around competitions; speed was prioritized over quality. I am planning to clean up this project as soon as time allows, see the below "Updating" section for more information.


## Starting the Dashboard

This project requires that you have NodeJS installed on your machine. Yarn is recommended as well, but not required of course. 
Run `npm start` or `yarn start` in the project's root folder to start the Dashboard at [http://localhost:3000](http://localhost:3000) in your browser.

The page will reload if you make edits.<br>
You will also see any errors in the browser's developer console (hit F12 to open).


## How it works

This Dashboard opens a websocket connection over port 5800 to a robot host and gets a tree of robot data in JSON format to display 3593's 2019 robot systems and capabilities on the screen in a "spacey", futuristic UI that we thought fit the "Deep Space" theme well.
To view a Java example of the robot-side websocket server implementation, see  [this class](https://gitlab.com/zeroClearAmerican/3593_2019_robot/blob/master/src/main/java/frc/robot/util/DashboardWebsocket.java) in our robot project from the same year.

During the season we found that this Dashboard architecture was stable, speedy, light on network bandwidth, easily customizable for adding new features, as well as very responsive to real-time changes coming in from the robot due to React's component structure and lifecycle. 


## History

This started off as an Electron project to wrap the React app and run as a desktop application with a TCP connection to the robot, however Electron felt far too clunky and the TCP connection was sketchy at best. Websockets provide a clean and stable connection with the ability to reconnect on failure and allowed us to ditch Electron altogether so the Dashboard can run in a browser window without issue.


## Updates

I'm planning to make a whitelabel version of this project with a base, non-specific example for teams to fork and try out themselves later on that will be much more clean and well-documented comment-wise. If you would like to be updated when that happens, shoot me an email at `emperor[at]emperorof[dot]space`.